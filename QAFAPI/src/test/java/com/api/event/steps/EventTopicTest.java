package com.api.event.steps;

import java.util.HashMap;
import java.util.Map;


import org.json.JSONObject;


import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.ws.rest.RestWSTestCase;

public class EventTopicTest extends RestWSTestCase {

	
	String response =null;
	@QAFTestStep(description = "user request to get all the user details")
	public void getRequest() {
		 System.out.println("Inside Get Request");
		 Map<String, Object> data = new HashMap<String, Object>();
	        data.put("endpointUrl", ConfigurationManager.getBundle().getProperty("get.endpoint"));
	        data.put("method", ConfigurationManager.getBundle().getProperty("get.method"));
	        data.put("header", ConfigurationManager.getBundle().getProperty("get.header1"));
	        WsStep.userRequests("get.eventtopic", data);
	        response = getResponse().getMessageBody();
   }

	@QAFTestStep(description = "user should get detail response")
	public void checkResponce() {
		System.out.println("Inside checkResponce");
		System.out.println("Response : "+response);
        JSONObject jsonobject =new JSONObject(response);
        System.out.println("Json Object -ID : "+jsonobject.getString("data.id[1]"));
        System.out.println("Json Object -Name : "+jsonobject.getString("data.attributes[0].name"));
        WsStep.responseShouldHaveStatusCode(200);
        WsStep.responseShouldHaveStatus("ok");

	}
}
