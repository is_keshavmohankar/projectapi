package com.api.event.steps;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.step.WsStep;

import com.qmetry.qaf.automation.ws.rest.RestTestBase;

public class NoBddSteps {

	@Test
	public void getEventTopicDetails() {
		WsStep.userRequests("get.event.topics");//Reading from properties file repo.properties
		//WsStep.userRequests("get.eventtopic");//Reading from xml file request.xml
		WsStep.responseShouldHaveStatus("OK");
		WsStep.responseShouldHaveStatusCode(200);
		
		int statusCode=new RestTestBase().getResponse().getStatus().getStatusCode();
		Reporter.log("Status code is : "+statusCode);
	}
}