package com.api.jsonpathlibrary.testcases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class JSONPathLibraryTestCases {

	@BeforeClass
	public DocumentContext getJsonFilePath() throws FileNotFoundException {
		return JsonPath.parse(new FileInputStream(".\\resource\\testdata\\sample-data.json"));
	}

	@Test(description = "Test to display all users")
	public void displayAllUsers() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonAllUsersName = "$.[?(@.age>=0)].name";
		List<String> jsonAllUsers = jsonContext.read(jsonAllUsersName);
		System.out.println("All Users : " + jsonAllUsers);
		Assert.assertTrue(jsonAllUsers.contains("Snow Rodriguez"));
	}

	@Test(description = "Test to display all female users")
	public void displayAllFemaleUserName() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonFemaleUserNamePath = "$.[?(@.gender=='female')].name";
		List<String> jsonFemaleUserName = jsonContext.read(jsonFemaleUserNamePath);
		System.out.println("All Female Users are as follows");
		for (String name : jsonFemaleUserName) {
			System.out.println("User Name: " + name);
		}
		Assert.assertTrue(jsonFemaleUserName.contains("Ada Potter"));
	}

	@Test(description = "Test to display first two users only")
	public void displayFirstTwoUsers() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonFirstTwoUserNamePath = "$[?(@['id']== '0' || @['id'] == '1')].name";
		List<String> jsonFirstTwoUsersList = jsonContext.read(jsonFirstTwoUserNamePath);
		System.out.println("Display First two users" + " " + jsonFirstTwoUsersList);
		Assert.assertTrue(jsonFirstTwoUsersList.contains("Snow Rodriguez"));
	}

	@Test(description = "Test to display last two users")
	public void displayLastTwoUsers() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		// 1st approach
		String jsonLastTwoUsersPath = "$.[-2,-1]";//]?(@.id==20 ||@.id==21)].name";
		List<String> jsonLastTwoUsersList = jsonContext.read(jsonLastTwoUsersPath);
		System.out.println("Last two user names are : " + jsonLastTwoUsersList);
		// 2nd approach
		String jsonAllUsersPath = "$.[?(@.age>=0)].name";
		List<String> jsonAllUserList = jsonContext.read(jsonAllUsersPath);
		System.out.println("Last two user names");
		for (int i = jsonAllUserList.size(); i >= jsonAllUserList.size() - 1; i--) {
			System.out.println("User: " + String.valueOf(jsonAllUserList.get(i - 1)));
		}
		Assert.assertTrue(jsonAllUserList.contains("Jewel Mccoy"));
	}

	@Test(description = "Test to display total number of friends for first user")
	public void displayTotalFriendsOfFirstUser() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonFriendsOfFirstUser = "$.[0].friends[*].name";
		List<String> jsonTotalFriendsOfFirstUserList = jsonContext.read(jsonFriendsOfFirstUser);
		System.out.println("Friends of first user are : " + jsonTotalFriendsOfFirstUserList);
	}

	@Test(description = "Test to display all users whose age between 24 to 36")
	public void displayUsersHavingSpecificAge() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonSpecificAgeUsersPath = "$..[?(@.age>=24 && @.age<=36)].name";
		List<String> jsonSpecificAgeUsersList = jsonContext.read(jsonSpecificAgeUsersPath);
		System.out.println("Friends of first user are : " + jsonSpecificAgeUsersList);
	}
	
	@Test(description = "Test to display balance of male users")
	public void displayBalanceOfMaleUsers() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		String jsonMaleUserNamePath = "$.[?(@.gender=='male')].name";
		String jsonMaleUserBalancePath = "$.[?(@.gender=='male')].balance";
		List<String> jsonMaleUserNameList = jsonContext.read(jsonMaleUserNamePath);
		List<String> jsonMaleUserBalanceList = jsonContext.read(jsonMaleUserBalancePath);
		System.out.println("All Male Users with there Balance Info");
		System.out.println("Display all male Users" + " " + jsonMaleUserNameList);
		System.out.println("Display Balance Info" + " " + jsonMaleUserBalanceList);
		Assert.assertTrue(jsonMaleUserNameList.contains("Snow Rodriguez"));
	}
	
	@Test(description = "Test to display all users who lives in California")
	public void displayUsersOfCalifornia() throws Exception {
		DocumentContext jsonContext = getJsonFilePath();
		//Filter expensiveFilter = Filter.filter(Criteria.where("age").in(32,34));
		String jsonUserAddressPath = "$.[*].address";
		String jsonUserNamePath = "$.[*].name";
		List<String> jsonUserAddressList = jsonContext.read(jsonUserAddressPath);
		List<String> jsonUserNameList = jsonContext.read(jsonUserNamePath);
		System.out.println("Address of users ling in California");
		for (int i=0; i<jsonUserAddressList.size(); i++) {
			if(String.valueOf(jsonUserAddressList.get(i)).contains("California")) {
				System.out.println("Username: " + String.valueOf(jsonUserNameList.get(i)));
				System.out.println("Address: " + String.valueOf(jsonUserAddressList.get(i)));
			}
		}
	}
}
	   
	 