package com.api.restassured.testcases;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

public class EventTickets extends TestBaseSpecification {
	
	EventNewEvent event=new EventNewEvent();
	@Test(priority=1,enabled=true,description = "Test to create new ticket")
	public void createNewTicketTest() throws FileNotFoundException, IOException, ParseException {
		event.createNewEventTest();
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getTicketData(newEventId)).when()
				.post(readProperties("createTicket"));
		System.out.println("Response Body is : " + response.body().asString());
		newTicketId = response.jsonPath().getString("data.id");
		System.out.println("New Ticket Id: " + newTicketId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New ticket created successfully");
	}
	
	@Test(priority=2,enabled=true,description = "Test to delete ticket details")
	public void deleteTicketTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken)
				.when()
				.delete(readProperties("deleteTicket") + newTicketId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"),"Object successfully deleted");
		Reporter.log("Ticket deleted successfully"); 
	}
	
	@Test(priority=3,enabled=true,description = "Test to delete created event details")
	public void deleteEventTest() throws FileNotFoundException, IOException {
		event.deleteEventTest();
	}

}
