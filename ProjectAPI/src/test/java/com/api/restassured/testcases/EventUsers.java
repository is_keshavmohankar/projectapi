package com.api.restassured.testcases;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

import static io.restassured.RestAssured.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class EventUsers extends TestBaseSpecification {

	@Test(priority=1,description = "Test to display list of all the user")
	public void getAllUserDetailsTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(getCredentials().toJSONString()).when().get(readProperties("getUsers"));
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is: " + responseBody);
		String attributes = response.jsonPath().getString("data.type");
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(attributes, "user");
		Reporter.log("User details displyed properly");
	}

	@Test(priority=2,description = "Test to create new user")
	public void createNewUserTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getUserJsonObject()).when()
				.post(readProperties("createUser"));
		System.out.println("Response Body is : " + response.body().asString());
		newUserId = response.jsonPath().getString("data.id");
		System.out.println("User id is: " + newUserId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New user created successfully");
	}

	@Test(priority=3,description = "Test to display Specific user details")
	public void getSpecificUserDetailsTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(getCredentials().toJSONString())
				.when().get(readProperties("getCreateduser") + newUserId);
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is: " + responseBody);
		String userId = response.jsonPath().getString("data.id");
		System.out.println("New User id is ---> :" + userId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(userId, newUserId);
		Reporter.log("User ID : "+userId+" displyed properly");
	}
	
	@Test(priority=4,description = "Test to update user details")
	public void updateUserTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.updateUserJsonObject(newUserId,"JohnUpdated", "TestUpdated").toJSONString()).when()
				.patch(readProperties("getCreateduser") + newUserId);
		System.out.println("Response Body is : " + response.body().asString());
		newUserId = response.jsonPath().getString("data.id");
		System.out.println("User id is: " + newUserId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("data.attributes.first-name"), "JohnUpdated");
		Reporter.log("User ID : "+ newUserId+ " name "+ response.jsonPath().getString("data.attributes.first-name") +" updated properly");
	}
	
	@Test(priority=5,description = "Test to delete user details")
	public void deleteUserTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken)
				.when()
				.delete(readProperties("getCreateduser") + newUserId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"),"Object successfully deleted");
		Reporter.log("User is deleted successfully"); 
	}
	
}