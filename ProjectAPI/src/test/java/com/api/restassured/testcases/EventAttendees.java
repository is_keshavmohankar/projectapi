package com.api.restassured.testcases;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

public class EventAttendees extends TestBaseSpecification {
	EventTickets ticket=new EventTickets();
	@Test(priority=1,description = "Test to create new attendees")
	public void createNewAttendeesTest() throws FileNotFoundException, IOException, ParseException {
		ticket.createNewTicketTest();
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getAttendessData(newEventId, newTicketId)).when()
				.post(readProperties("createAttendees"));
		System.out.println("Response Body is : " + response.body().asString());
		newAttendeesId = response.jsonPath().getString("data.id");
		System.out.println("New attendee is: " + newAttendeesId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New Attendees created successfully");
	}
	
	@Test(priority=2,description = "Test to update attendees")
	public void updateAttendeesTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.updateAttendessData(newAttendeesId)).when()
				.patch(readProperties("updateAttendees") + newAttendeesId);
		System.out.println("Response Body is : " + response.body().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
		Reporter.log("Attendees Updated successfully");
	}
	
	@Test(priority=3,enabled=true,description = "Test to delete attendees details")
	public void deleteAttendeesTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken)
				.when()
				.delete(readProperties("deleteAttendees") + newAttendeesId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"),"Object successfully deleted");
		Reporter.log("Attendees deleted successfully"); 
		
	}
	
	@Test(priority=4,enabled=true,description = "Test to delete created Ticket And Event details")
	public void deleteTicketAndEventTest() throws FileNotFoundException, IOException {
		ticket.deleteTicketTest();
		ticket.deleteEventTest();
	}
	
	
	
}
