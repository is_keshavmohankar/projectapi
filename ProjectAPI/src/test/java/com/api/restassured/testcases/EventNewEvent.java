package com.api.restassured.testcases;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

public class EventNewEvent extends TestBaseSpecification{
	
	@Test(description = "Test to get order details")
	public void getAllEventTest() throws FileNotFoundException, IOException  {
		 response=given()
			.header("Accept", "application/vnd.api+json")
			.header("Authorization",jwtToken)
			.when()
			.get(readProperties("getEvents"));
		 responseBody = response.body().asString();
		 System.out.println("Response body : "+responseBody);
		 System.out.println("Response Status : "+response.getStatusCode());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 Reporter.log("Status code is 200 (OK)");
	}
	
	
	@Test(description = "Test to create new Event")
	public void createNewEventTest() throws FileNotFoundException, IOException, ParseException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getJsonFileData())
				.when()
				.post(readProperties("createEvent"));
		System.out.println("Request url :"+readProperties("createEvent"));
		System.out.println("Request Payload :"+Payload.getEventData());
		System.out.println("Response Body is : " + response.body().asString());
		newEventId = response.jsonPath().getString("data.id");
		System.out.println("New Event Id: " + newEventId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New Event created successfully");
	}
	
	@Test(priority=3,enabled=true,description = "Test to delete ticket details")
	public void deleteEventTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken)
				.when()
				.delete(readProperties("deleteEvent") + newEventId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"),"Object successfully deleted");
		Reporter.log("Event deleted successfully"); 
	}
}
