package com.api.restassured.testcases;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.api.restassured.testbase.TestBaseSpecification;
import static io.restassured.RestAssured.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;


public class EventLocations extends TestBaseSpecification {
	
	@BeforeClass
	public void getEventLocationsTest() throws FileNotFoundException, IOException {
		basePath = readProperties("getEventLocations");
		response = given().header("Content-Type", "application/json").when().get(basePath);
		responseBody = response.body().asString();
		JsonPath jsonPath = response.jsonPath();
		Assert.assertFalse(responseBody.isEmpty());
		System.out.println("Response Body : " + responseBody);
		System.out.println("Display event meta information using Json path : " + jsonPath.get("meta"));
		System.out.println("Dispay Event details using Json path : " + jsonPath.get("data[0]"));
		Reporter.log("Response details displyed properly");
	}

	@Test(description = "Test to check Status Code of the response")
	public void checkResponseStatusCodeTest() {
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		Reporter.log("Status code is 200 (OK)");
	}

	@Test(description = "Test to check status of the response body")
	public void checkResponseBodyTest() {
		if (!(responseBody.isEmpty())) {
			Reporter.log("Response Body is having detail information");
		} else
			Reporter.log("Response Body is empty ");
	}

	@Test(description = "Test to displat all headers of the response")
	public void checkResponseHeadersTest() {
		Headers allHeaders = response.headers();
		System.out.println("Header Details are As follows");
		for (Header header : allHeaders) {
			System.out.println(header.getName() + " : " + header.getValue());
		}
		Reporter.log("Headers are displayed properly");
	}

	@Test(description = "Test to check Status line of the response")
	public void checkResponseStatusLineTest() {
		String statusLine = response.getStatusLine();
		Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");
		Reporter.log("Status Line is correct");
	}

	@Test(description = "Test to check content type of the response")
	public void checkResponseContentTypeTest() {
		String contentType = response.getHeader("Content-Type");
		Assert.assertEquals(contentType, "application/vnd.api+json");
		Reporter.log("Content Types are correct");
	}

	@Test(description = "Test to check header of the response")
	public void checkResponseServerTypeTest() {
		String serverType = response.getHeader("Server");
		Assert.assertEquals(serverType, "nginx/1.17.6");
		Reporter.log("Server Types is displayed properly");
	}
	
	@Test(description = "Test to check type of the data displayed in the response")
	public void checkResponseDataTypeTest() {
		String dataType = response.jsonPath().getString("data.type[0]");
		Assert.assertEquals(dataType, "event-location");
		Reporter.log("Data Types "+dataType+" is displayed properly");
	}
	
	@Test(description = "Test to check attribute name present in the response data")
	public void checkAttributesNameTest() {
		String attributeName = response.jsonPath().getString("data.attributes[0].name");
		Assert.assertEquals(attributeName, "infostretch");
		Reporter.log("Attribute "+attributeName+" is displayed properly");
	}
	
	@Test(description = "Test to check attribute name present in the response data")
	public void checkSpecificAttributesNameTest() {
		String attributeName = response.jsonPath().getString("data.attributes[3].name");
		Assert.assertEquals(attributeName, "Bangalore");
		Reporter.log("Attribute "+attributeName+" is displayed properly");
	}
	
	@Test(priority = 11, description = "Test to check ID of the data displayed in the response")
	public void checkResponseDataIDTest() {
		String dataId= response.jsonPath().getString("data.id[1]");
		Assert.assertEquals(dataId, "1");
		Reporter.log("Data Id "+dataId+" is displayed properly");
	}
}
