package com.api.restassured.testcases;

import static io.restassured.RestAssured.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

public class EventOrders extends TestBaseSpecification {
	
	EventAttendees attendees=new EventAttendees();
	@Test(priority=1,description = "Test to get order details")
	public void getAllOrdersTest() throws FileNotFoundException, IOException  {
		 response=given()
			.header("Accept", "application/vnd.api+json")
			.header("Authorization",jwtToken)
			.when()
			.get(readProperties("getOrders"));
		 responseBody = response.body().asString();
		 System.out.println("Response body : "+responseBody);
		 System.out.println("Response Status : "+response.getStatusCode());
		 Assert.assertEquals(response.getStatusCode(), 200);
		 Reporter.log("Status code is 200 (OK)");
	}

	@Test(enabled=true,priority=2,description = "Test to post order request")
	public void createNewOrderTest() throws FileNotFoundException, IOException, ParseException {
		attendees.createNewAttendeesTest();
		response=given()
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.body(Payload.getOrderJsonObject(newAttendeesId, newTicketId))
				.when()
				.post(readProperties("getOrders"));
		 responseBody = response.body().asString();
		 orderId=response.jsonPath().getString("data.id");
		 System.out.println("Response body : "+responseBody);
		 System.out.println("New Order Id : "+orderId);
		 System.out.println("Response Status : "+response.getStatusCode());
		 Assert.assertEquals(response.getStatusCode(), 201);
		 Reporter.log("Order created successfully");
	}
	@Test(priority=3,description = "Test to delete user details")
	public void deleteOrderTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken)
				.when()
				.delete(readProperties("deleteOrders") + orderId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"),"Object successfully deleted");
		Reporter.log("Order deleted successfully"); 
	}
	
	@Test(priority=4,enabled=true,description = "Test to delete created Ateendees Ticket And EventTest details")
	public void deleteAteendeesTicketAndEventTest() throws FileNotFoundException, IOException {
		attendees.deleteAttendeesTest();
		new EventTickets().deleteTicketTest();
		new EventNewEvent().deleteEventTest();
	}
	
}
