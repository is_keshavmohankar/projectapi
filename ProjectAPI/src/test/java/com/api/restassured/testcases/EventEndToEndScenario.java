package com.api.restassured.testcases;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;

public class EventEndToEndScenario extends TestBaseSpecification {
	
	@Test(priority = 1, description = "Test to create new Event")
	public void createNewEventTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getEventData()).when().post(readProperties("createEvent"));
		newEventId = response.jsonPath().getString("data.id");
		System.out.println("Event Id: " + newEventId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New Event created successfully");
	}

	@Test(priority = 2, enabled = true, description = "Test to create new ticket")
	public void createNewTicketTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getTicketData(newEventId)).when().post(readProperties("createTicket"));
		newTicketId = response.jsonPath().getString("data.id");
		System.out.println("Ticket Id: " + newTicketId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New ticket created successfully");
	}

	@Test(priority= 3,description = "Test to update attendees")
	public void updateTicketTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.updateTicketData(newTicketId)).when()
				.patch(readProperties("updateTicket") + newTicketId);
		System.out.println("Response Body is : " + response.body().asString());
		Assert.assertEquals(response.getStatusCode(), 200);
		Reporter.log("Ticket Updated successfully");
	}
	@Test(priority = 4, enabled = true, description = "Test to create new attendees")
	public void createNewAttendeesTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getAttendessData(newEventId, newTicketId)).when().post(readProperties("createAttendees"));
		newAttendeesId = response.jsonPath().getString("data.id");
		System.out.println("User attendee is: " + newAttendeesId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("New Attendees created successfully");
	}

	@Test(priority = 5, enabled = true, description = "Test to post order request")
	public void createNewOrderTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json").header("Authorization", jwtToken)
				.body(Payload.getOrderJsonObject(newAttendeesId, newTicketId)).when()
				.post(readProperties("getOrders"));
		responseBody = response.body().asString();
		orderId = response.jsonPath().getString("data.id");
		System.out.println("Response body : " + responseBody);
		System.out.println("New Order Id : "+orderId);
		Assert.assertEquals(response.getStatusCode(), 201);
		Reporter.log("Order created successfully");
	}

	@Test(priority = 6, description = "Test to delete user details")
	public void deleteOrderTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken).when()
				.delete(readProperties("deleteOrders") + orderId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"), "Object successfully deleted");
		Reporter.log("Order deleted successfully");
	}

	@Test(priority = 7, enabled = true, description = "Test to delete attendees details")
	public void deleteAttendeesTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken).when()
				.delete(readProperties("deleteAttendees") + newAttendeesId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"), "Object successfully deleted");
		Reporter.log("Attendees deleted successfully");
	}

	@Test(priority = 8, enabled = true, description = "Test to delete ticket details")
	public void deleteTicketTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken).when()
				.delete(readProperties("deleteTicket") + newTicketId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"), "Object successfully deleted");
		Reporter.log("Ticket deleted successfully");
	}

	@Test(priority = 9, enabled = true, description = "Test to delete ticket details")
	public void deleteEventTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json").header("Authorization", jwtToken).when()
				.delete(readProperties("deleteEvent") + newEventId);
		Assert.assertEquals(response.getStatusCode(), 200);
		Assert.assertEquals(response.jsonPath().getString("meta.message"), "Object successfully deleted");
		Reporter.log("Event deleted successfully");
	}
	EventOrders order=new EventOrders();
//	@Test(priority = 1, description = "Test")
//	public void createEventEndToEndScenarioTest() throws FileNotFoundException, IOException {
//		order.createNewOrderTest();
//	}
//	
//	@Test(priority = 2, description = "Test")
//	public void deleteEventEndToEndScenarioTest() throws FileNotFoundException, IOException {
//		order.deleteOrderTest();
//		order.deleteAteendeesTicketAndEventTest();
//	}
	
}
