package com.api.restassured.testcases;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.api.restassured.supportedfunctions.Payload;
import com.api.restassured.testbase.TestBaseSpecification;
import static io.restassured.RestAssured.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;


public class EventTopics extends TestBaseSpecification {
	String responseBody=null;
	@Test(description="Test to display all the event topics")
	public void getEventTopicsTest() throws FileNotFoundException, IOException {
		response = given().header("Accept", "application/vnd.api+json")
				.header("Authorization",jwtToken).when().get(readProperties("getEventTopics"));
		responseBody = response.getBody().asString();
		JsonPath jsonPath = response.jsonPath();
		System.out.println("Responce Body contains : " + responseBody);
		int statusCode = response.getStatusCode();
		System.out.println("Dispay Event Topic details using Json path : " + jsonPath.get("data[0].type"));
		Assert.assertEquals(statusCode, 200, "Request failed - status code : " + statusCode);
		Assert.assertEquals(jsonPath.get("data[0].type"), "event-topic");
		Reporter.log("Event Topics are displayed properly"); 
	}

	@Test(description="Test to display specific event topic")
	public void getSpecificEventTopicTest() throws FileNotFoundException, IOException {
		response = given().header("Content-Type", "application/vnd.api+json")
				.header("Authorization",jwtToken).when().get(readProperties("getSpecificEventTopics"));

		responseBody = response.getBody().asString();
		JsonPath jsonPath = response.jsonPath();
		System.out.println("Responce Body contains : " + responseBody);
		int statusCode = response.getStatusCode();
		System.out.println("Dispay event type using Json path : " + jsonPath.get("data.type"));
		Assert.assertEquals(statusCode, 200, "Request failed - status code : " + statusCode);
		Assert.assertEquals(jsonPath.get("data.type"), "event-topic");
		Assert.assertEquals(response.getStatusLine(),"HTTP/1.1 200 OK","Request failed - status line : "+response.getStatusLine());
		Reporter.log("Specifc Event topic is displayed properly");
	}

	@SuppressWarnings("unchecked")
	@Test(description = "Test to update event topic")
	public void updateEventTopicTest() throws FileNotFoundException, IOException {
		JSONObject requestParams = new JSONObject();
		requestParams.put("data", Payload.getEventTopicsJsonObject());
		response = given()
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.body(requestParams.toJSONString())
				.patch(readProperties("patchEvent"));
		System.out.println("Final Json in request body : " + requestParams.toJSONString());
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is: " + responseBody);
		System.out.println("Response Status code is: " + response.getStatusCode());
		Assert.assertEquals(response.getStatusCode(), 200);
		Reporter.log("Event Topic is updated successfully");
	}
	
	@Test(enabled=false,description = "Test to delete specific event topic")
	public void deleteEventTopicTest() {
		RestAssured.baseURI = "http://qe.events.infostretch.com/api";
		RestAssured.basePath = "v1/events/12";

		given().header("Content-Type", "application/json").header("Authorization",jwtToken)
				.body(getCredentials().toJSONString()).when().delete().then().statusLine("HTTP/1.1 200 OK")
				.statusCode(200).log().all();
		Reporter.log("Event Topic delete successfully");
	}

	
}
