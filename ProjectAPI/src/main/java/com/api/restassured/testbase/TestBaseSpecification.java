package com.api.restassured.testbase;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TestBaseSpecification {
	public static String jwtToken;
	public Logger logger;
	protected static RequestSpecification httpRequest;
	private static JSONObject requestParams;
	public static Response response = null;
	public static String responseBody;
	static String filePath;
	public  String newEventId = null;
	public  String newAttendeesId = null;
	public  String orderId = null;
	public  String newTicketId = null;
	public String newUserId = null;

	@BeforeSuite
	public static void getBaseUrl() {
		try {
			baseURI = readProperties("baseURI");
		} catch (Exception e) {
			e.printStackTrace();
		}
		jwtToken = "JWT" + " " + getJWTToken();
	}

	@SuppressWarnings("unchecked")
	@BeforeClass
	public static JSONObject getCredentials() {
		requestParams = new JSONObject();
		try {
			requestParams.put("email", readProperties("adminEmail"));
			requestParams.put("password", readProperties("adminPassword"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return requestParams;
	}

	public static String getJWTToken() {
		try {
			response = given().header("Content-Type", "application/json").body(getCredentials().toJSONString()).when()
					.post(readProperties("postEndPoint"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(response.body().asString());
		if ((response.getStatusCode()) == 200) {
			jwtToken = response.jsonPath().get("access_token");
		}
		return jwtToken;
	}

	public static Properties getPropertiesFileObj() throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		filePath = System.getProperty("user.dir") + "\\resource\\testdata\\application.properties";
		prop.load(new FileInputStream(new File(filePath)));
		return prop;
	}

	public static String readProperties(String input) throws FileNotFoundException, IOException {
		Properties prop = getPropertiesFileObj();
		return prop.getProperty(input);
	}

}
