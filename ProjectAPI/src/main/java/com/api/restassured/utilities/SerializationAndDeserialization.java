package com.api.restassured.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


@SuppressWarnings("serial")
class Test implements Serializable{
	int i=10,j=20;
	String message="Test Serializable";
}

public class SerializationAndDeserialization {
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Test t1=new Test();
		//Serialization
		FileOutputStream fos= new FileOutputStream("D:\\API Automation\\text.txt");
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		System.out.println("Writing objet into file..........");
		oos.writeObject(t1);
		oos.close();
		fos.close();
		
		//DeSerialization
		FileInputStream fis=new FileInputStream("D:\\API Automation\\text.txt");
		ObjectInputStream ois= new ObjectInputStream(fis);
		Test t2=(Test)ois.readObject();
		System.out.println("...Object Details....");
		System.out.println("I : "+t2.i+" J : "+t2.j+" Message : "+t2.message);
		ois.close();
		fis.close();		
	}
}

