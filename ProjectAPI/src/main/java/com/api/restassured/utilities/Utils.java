package com.api.restassured.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class Utils {
	
	public static FileInputStream fileInput;
	public static FileOutputStream fileOutput;
	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;
	public static XSSFRow row;
	public static XSSFCell cell;
	
	
	@DataProvider(name="postdatarequest1")
	public String[][] getData() {
		String data[][]= {{"A","1"},{"B","2"}};
		return data;
	}
	
	public static int getRowCount(String xlFile,String xlsheet) throws IOException {
		fileInput=new FileInputStream(xlFile);
		workbook=new XSSFWorkbook(fileInput);
		sheet=workbook.getSheet(xlsheet);
		int rowCount=sheet.getLastRowNum();
		workbook.close();
		fileInput.close();
		return rowCount;
	}
	public static int getCellCount(String xlFile,String xlsheet,int rowNum) throws IOException {
		fileInput=new FileInputStream(xlFile);
		workbook=new XSSFWorkbook(fileInput);
		sheet=workbook.getSheet(xlsheet);
		row=sheet.getRow(rowNum);
		int cellCount=row.getLastCellNum();
		workbook.close();
		fileInput.close();
		return cellCount;
	}
	public static void readExcel(String filePath,String sheetName) throws IOException{
		File file=new File(filePath);
		fileInput=new FileInputStream(file);
		workbook= new XSSFWorkbook(fileInput);
		sheet=workbook.getSheet(sheetName);
		int rowNo=sheet.getLastRowNum();
		System.out.println("Last Row num :" +rowNo);
		for(int i=1;i<rowNo;i++) {
			row=sheet.getRow(i);
			for(int j=0;j<row.getLastCellNum();j++) {
				System.out.print(row.getCell(j).getStringCellValue() +" | ");
			}
			System.out.println();
		}
		workbook.close();
		fileInput.close();
	}
	public static void writeExcel(String filePath,String sheetName, String data[]) throws IOException{
	File file=new File(filePath);
	fileInput=new FileInputStream(file);
	workbook=new XSSFWorkbook(fileInput);
	sheet=workbook.getSheet(sheetName);
	int rowCount=sheet.getLastRowNum();
	row=sheet.getRow(0);
	XSSFRow newRow=sheet.createRow(rowCount+1);
	XSSFCell cell;
	for(int j=0;j<row.getLastCellNum();j++){
		 cell=newRow.createCell(j);
		 cell.setCellValue(data[j]);
	}
	fileInput.close();

	FileOutputStream fileOutput=new FileOutputStream(file);
	workbook.write(fileOutput);
	workbook.close();
	fileOutput.close();
	}
		
//	public static void main(String[] args) throws IOException {
//		System.out.print("-----Read Before write-------");
//		readExcel("C:\\Users\\keshav.mohankar\\Documents\\Test.xlsx","Sheet1");
//		String obj[]= {"E","Goa"};
//		writeExcel("C:\\Users\\keshav.mohankar\\Documents\\Test.xlsx","Sheet1",obj);
//		System.out.print("-----Read After write-------");
//		readExcel("C:\\Users\\keshav.mohankar\\Documents\\Test.xlsx","Sheet1");
//	}
	
}
