package com.infostretch.api.jersey.payload;

import org.json.simple.JSONObject;

public class DataPayload {

	public static String getCredentials() {
				String credentials="{   \"email\": \"admin@mailinator.com\",   \"password\": \"admin123#\",    \"remember-me\": true,    \"include-in-response\": true }";
		return credentials;
	}
	
	public static String getOrderJsonObject(String newAttendeesId,String newTicketId) {
		String orderData="{ 			  \"data\": { 			    \"attributes\": { 			      \"payment-mode\": \"paypal\", 			      \"country\": \"India\", 			      \"status\": \"pending\", 			      \"amount\": \"10\", 			      \"order-notes\": \"example\" 			    }, 			    \"type\": \"order\", 			    \"relationships\": { 			      \"attendees\": { 			        \"data\": [ 			          { 			            \"id\": \""+newAttendeesId+"\", 			            \"type\": \"attendee\" 			          } 			        ] 			      }, 			      \"event\": { 			        \"data\": { 			          \"id\": \""+newTicketId+"\", 			          \"type\": \"event\" 			        } 			      } 			    } 			  } 			} ";
		return orderData;
	}

	public static String getAttendessData(String newEventId,String newTicketId){
    	String attendessData ="{\"data\": { 			    \"attributes\": { 			      \"firstname\": \"firstName\", 			      \"lastname\": \"lastName\", 			      \"email\": \"johndoe1@example.com\", 			      \"address\": \"address\", 			      \"city\": \"example\", 			      \"state\": \"example\", 			      \"country\": \"IN\", 			      \"is-checked-in\": \"false\", 			      \"attendee-notes\": \"example\", 			      \"pdf-url\": \"http://example.com\" 			    }, 			    \"type\": \"attendee\", 			    \"relationships\": { 			      \"event\": { 			        \"data\": { 			          \"id\": \""+newEventId+"\", 			          \"type\": \"event\" 			        } 			      }, 			      \"ticket\": { 			        \"data\": { 			          \"id\": \""+newTicketId+"\", 			          \"type\": \"ticket\" 			        } 			      } 			    } 			  } 			}";
    return attendessData;
    }
    
    public static String updateAttendessData(String newAttendeesId){
    		String attendessData ="{   \"data\": {     \"attributes\": {       \"firstname\": \"Updated firstName\",       \"lastname\": \"Updated lastName\",       \"email\": \"johndoe@example.com\",       \"address\": \"address\",       \"city\": \"example\",       \"state\": \"example\",       \"country\": \"IN\",       \"is-checked-in\": \"true\",       \"checkin-times\": \"2017-12-13T22:59:59.123456+00:00\",       \"attendee-notes\": \"example\",       \"pdf-url\": \"http://example.com\"     },     \"type\": \"attendee\",     \"id\": \""+newAttendeesId+"\"   } }";
        return attendessData;
    }
    
    public static String getTicketData(String newEventId){
    	String ticketData ="{ 				  \"data\": { 				    \"relationships\": { 				      \"event\": { 				        \"data\": { 				          \"type\": \"event\", 				          \"id\":\""+newEventId+"\" 				        } 				      } 				    }, 				    \"attributes\": { 				      \"name\": \"Ticket for {{eventName}}\", 				      \"description\": \"Ticket for {{eventName}}\", 				      \"type\": \"VIP-ticket\", 				      \"price\": \"100.00\", 				      \"quantity\": \"100\", 				      \"is-description-visible\": \"false\", 				      \"is-checkin-restricted\": \"true\", 				      \"auto-checkin-enabled\": \"false\", 				      \"position\": \"1\", 				      \"is-fee-absorbed\": \"false\", 				      \"sales-starts-at\": \"2017-06-01T01:24:47.500127+00:00\", 				      \"sales-ends-at\": \"2017-06-30T00:24:47.500127+00:00\", 				      \"is-hidden\": \"false\", 				      \"min-order\": \"1\", 				      \"max-order\": \"100\" 				    }, 				    \"type\": \"ticket\" 				  } 				}";
    return ticketData;
    }
    
    public static String updateTicketData(String newTicketId){
		String ticketData ="{   \"data\": {     \"attributes\": {       \"name\": \"Updated-ticket\",       \"description\": \"some description\",       \"type\": \"VIP-ticket\",       \"price\": \"100.00\",       \"quantity\": \"100\",       \"is-description-visible\": \"false\",       \"is-checkin-restricted\": \"true\",       \"auto-checkin-enabled\": \"false\",       \"position\": \"1\",       \"is-fee-absorbed\": \"false\",       \"sales-starts-at\": \"2017-06-01T01:24:47.500127+00:00\",       \"sales-ends-at\": \"2017-06-30T00:24:47.500127+00:00\",       \"is-hidden\": \"false\",       \"min-order\": \"5\",       \"max-order\": \"100\"     },     \"type\": \"ticket\",     \"id\": \""+newTicketId+"\"   } }";
    return ticketData;
    }
    
    public static String getEventData(){
    	String eventData ="{ 				  \"data\": { 				    \"attributes\": { 				      \"name\": \"ownerName\", 				      \"external-event-url\": \"http://Sage.com\", 				      \"starts-at\": \"2099-12-13T23:59:59.123456+00:00\", 				      \"ends-at\": \"2099-12-14T23:59:59.123456+00:00\", 				      \"timezone\": \"UTC\", 				      \"latitude\": \"1.23456789\", 				      \"longitude\": \"1.23456789\", 				      \"logo-url\": \"http://example.com/example.png\", 				      \"location-name\": \"example\", 				      \"searchable-location-name\": \"example\", 				      \"description\": \"example\", 				      \"original-image-url\": \"https://www.w3schools.com/html/pic_mountain.jpg\", 				      \"owner-name\": \"ownerName\", 				      \"is-map-shown\": \"true\", 				      \"owner-description\": \"example\", 				      \"is-sessions-speakers-enabled\": \"true\", 				      \"privacy\": \"public\", 				      \"state\": \"draft\", 				      \"is-event-online\": false, 				      \"average-rating\": null, 				      \"ticket-url\": \"http://example.com\", 				      \"code-of-conduct\": \"example\", 				      \"is-ticketing-enabled\": \"true\", 				      \"payment-country\": \"US\", 				      \"payment-currency\": \"USD\", 				      \"paypal-email\": \"example@example.com\", 				      \"is-tax-enabled\": \"true\", 				      \"can-pay-by-paypal\": \"false\", 				      \"can-pay-by-stripe\": \"false\", 				      \"can-pay-by-cheque\": \"false\", 				      \"can-pay-by-bank\": \"false\", 				      \"can-pay-by-omise\": \"false\", 				      \"can-pay-onsite\": \"true\", 				      \"cheque-details\": \"example\", 				      \"bank-details\": \"example\", 				      \"onsite-details\": \"example\", 				      \"is-sponsors-enabled\": \"false\", 				      \"has-owner-info\": \"false\" 				    }, 				    \"type\": \"event\" 				  } 				}";
    return eventData;
    }
  
    public static String getUserJsonObject() {
    	String userData="{   \"data\": {     \"attributes\": {       \"email\": \"testUser1@example.com\",       \"password\": \"password\",       \"avatar_url\": \"http://example.com/example.png\",       \"first-name\": \"TestJohn1\",       \"last-name\": \"TestDoe1\",       \"details\": \"example\",       \"contact\": \"example\",       \"facebook-url\": \"http://facebook.com/facebook\",       \"twitter-url\": \"http://twitter.com/twitter\",       \"instagram-url\": \"http://instagram.com/instagram\",       \"google-plus-url\": \"http://plus.google.com/plus.google\",       \"original-image-url\": \"https://cdn.pixabay.com/photo/2013/11/23/16/25/birds-216412_1280.jpg\"     },     \"type\": \"user\"   } }";
    	return userData;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject updateUserJsonObject(String id,String firstName,String LastName) {
		JSONObject requestParamsNested = new JSONObject();
		requestParamsNested.put("first-name",firstName);
		requestParamsNested.put("last-name", LastName);
		
		JSONObject requestParams1 = new JSONObject();
		requestParams1.put("attributes", requestParamsNested);
		requestParams1.put("type", "user");
		requestParams1.put("id", id);
		JSONObject requestParams = new JSONObject();
		requestParams.put("data", requestParams1);
		return requestParams;
	}
	@SuppressWarnings("unchecked")
	public static JSONObject getEventTopicsJsonObject() {
		JSONObject requestParamsNested = new JSONObject();
		requestParamsNested.put("name", "Travel and food1");
		JSONObject requestParams = new JSONObject();
		requestParams.put("attributes",requestParamsNested);
		requestParams.put("id", "147");
		requestParams.put("type", "event-topic");
		return requestParams;
	}
    
}
