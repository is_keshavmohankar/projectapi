package com.infostretch.api.jersey.testbase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import com.infostretch.api.jersey.payload.DataPayload;
import com.jayway.jsonpath.JsonPath;

public class TestBaseSpecification {

	public Response response;
	public String newUserId=null;
	public static String newEventId=null;
	public static String jwtToken=null;
	public static String newTicketId=null;
	public static String newAttendeesId=null;  
	public static String newOrderId=null;
	String url;

	@BeforeSuite
	public WebTarget createClientRequest() {
		
		Client client = ClientBuilder.newClient();
		WebTarget baseTarget = client.target(getUri());
		
		return baseTarget;
	}
	public UriBuilder getUri(){
		try {
			url=readProperties("baseURI");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return UriBuilder.fromUri(url);
	}
	
	@BeforeClass
	public void generateToken() throws FileNotFoundException, IOException {
		jwtToken = "JWT" + " " + createJwtToken();
	}

	public String createJwtToken() throws FileNotFoundException, IOException{
		response=createClientRequest()
				.path(readProperties("postEndPoint"))
				.request() 
				.header("Content-Type", "application/json")
				.post(Entity.entity(DataPayload.getCredentials(), MediaType.valueOf("application/json")));
				String message = response.readEntity(String.class);
				jwtToken=JsonPath.parse(message).read("access_token");
				System.out.println("Access_token is : " + jwtToken);
				Assert.assertEquals(response.getStatus(),200);
				Reporter.log("Access_token generated successfully");
				return jwtToken;
	}
	public static Properties getPropertiesFileObj() throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		String filePath = System.getProperty("user.dir") + "\\test-data\\application.properties";
		prop.load(new FileInputStream(new File(filePath)));
		return prop;
	}

	public static String readProperties(String input) throws FileNotFoundException, IOException {
		Properties prop = getPropertiesFileObj();
		return prop.getProperty(input);
	}
}
