package com.infostretch.api.jersey.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infostretch.api.jersey.model.Message;
import com.infostretch.api.jersey.service.MessageService;

@Path("messages")
public class MyResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Message> getMessages() {
		return  MessageService.getAllMessages();
	}

	@GET
	@Path("/{messageId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage(@PathParam("messageId") int messageId) {
		Message message = MessageService.getSpecificMessage(messageId);
		return Response.ok().entity(message).build();

	}

	@DELETE
	@Path("/{messageId}")
	public Response deleteEmployeeById(@PathParam("messageId") Integer id) {
		return Response.status(202).entity("Employee deleted successfully !!").build();
	}

	@POST
	@Path("/message")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(Message message) {
		if (message == null) {
			return Response.status(400).entity("Please add message details !!").build();
		}

		if (message.getMessage() == null) {
			return Response.status(400).entity("Please provide the message  text details !!").build();
		}
		return Response.status(201).entity("Message Created").build();

	}
	
	 @PUT
	 @Path("/update/{id}")
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response updateMessage(@PathParam("messageId") Integer id,Message message) {
		 	Message updatedMessage = new Message();
	        if(message.getAuthor()== null || message.getAuthor()==null ) {
		        return Response.status(400).entity("Please provide the author name !!").build();
		    }
		    updatedMessage.setId(id); 
		    updatedMessage.setMessage(message.getMessage());
		    updatedMessage.setAuthor(message.getAuthor());
		    return Response.ok().entity(updatedMessage).build();
	 }

}