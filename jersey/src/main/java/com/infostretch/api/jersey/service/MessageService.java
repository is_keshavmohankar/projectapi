package com.infostretch.api.jersey.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.infostretch.api.jersey.model.Message;

public class MessageService {

	
	public static List<Message> getAllMessages() {
		List<Message> list= new ArrayList<Message>();
		list.add(new Message(111, "Message 1", "Test1"));
		list.add(new Message(112, "Message 2", "Test2"));
		list.add(new Message(113, "Message 3", "Test3"));
		list.add(new Message(114, "Message 4", "Test4"));
		list.add(new Message(115, "Message 5", "Test5"));
		return list;
	}

	public static Message getSpecificMessage(int messageId) {
		Message obj;
		if (messageId <= 0) {
			return null;
		} else {
			List<Message> list = getAllMessages();
			Iterator<Message> itr = list.iterator();
			while (itr.hasNext()) {
				obj = itr.next();
				if (obj.getId() == messageId) {
					return obj;
				}
			}
		}
		return null;
	}
}
