package com.infostretch.api.jersey.client;

import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.infostretch.api.jersey.model.Message;

public class RestfulClientServer {

	Response responseMessage;

	@BeforeClass
	public WebTarget createClientRequest() {
		Client client = ClientBuilder.newClient();
		WebTarget baseTarget = client.target("http://localhost:8080/jersey/webapi");
		WebTarget messageTarget = baseTarget.path("/messages");
		return messageTarget;
	}


	@Test(description = "Test to get all message details",priority=2)
	public void getAllMessagesRequest() {
		WebTarget messageTarget = createClientRequest();
		responseMessage = messageTarget.request(MediaType.APPLICATION_JSON).get(Response.class);
		String message = responseMessage.readEntity(String.class);
		System.out.println("---------------------");
		System.out.println("Message Reponse : " + responseMessage.getStatus());
		System.out.println("Message Body : " + responseMessage.toString());
		System.out.println("Message details : " + message);
		System.out.println("---------------------");
	}

	@Test(description = "Test to get specific message details",enabled=true)
	public void getSpecificMessageRequest() {
		WebTarget specificMessageTarget = createClientRequest().path("/114");
		responseMessage = specificMessageTarget.request(MediaType.APPLICATION_JSON).get();
		JsonObject message = responseMessage.readEntity(JsonObject.class);
		System.out.println("---------------------");
		System.out.println("Message Reponse : " + responseMessage.getStatus());
		System.out.println("Message Body : " + responseMessage.toString());
		System.out.println("Message details : " + message);
		System.out.println("Message Id : " + message.getInt("id"));
		System.out.println("---------------------");
		Assert.assertEquals(message.getInt("id"), 114);

	}

	@Test(description = "Test to create new message", priority=1)
	public void postMessageRequest() {
		// Post request
		Message newMessage = new Message();
		newMessage.setId(116);
		newMessage.setMessage("Message 6");
		newMessage.setAuthor("Test6");
		WebTarget messageTarget = createClientRequest().path("/message");
		responseMessage = messageTarget.request().post(Entity.json(newMessage));
		System.out.println("---------------------");
		System.out.println("Message Reponse : " + responseMessage);
		System.out.println("Message Status : " + responseMessage.getStatus());
		System.out.println("---------------------");
	}
	
	@Test(description = "Test to update message", priority=3)
	public void putMessageRequest() {
		// Post request
		Message newMessage = new Message();
		newMessage.setId(117);
		newMessage.setMessage("Message 7");
		newMessage.setAuthor("Test7");
		WebTarget messageTarget = createClientRequest().path("/update").path("/115");
		responseMessage = messageTarget.request().put(Entity.json(newMessage));
	//	Message message=responseMessage.readEntity(Message.class);
		System.out.println("---------------------");
		System.out.println("Message Reponse : " + responseMessage);
		System.out.println("Message Status : " + responseMessage.getStatus());
		//System.out.println("Updated Message : " +message);
		System.out.println("---------------------");
	}
}
