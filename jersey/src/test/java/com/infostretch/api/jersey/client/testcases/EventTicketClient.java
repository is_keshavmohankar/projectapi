package com.infostretch.api.jersey.client.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.infostretch.api.jersey.payload.DataPayload;
import com.infostretch.api.jersey.testbase.TestBaseSpecification;
import com.jayway.jsonpath.JsonPath;

public class EventTicketClient extends TestBaseSpecification {

	EventNewEventClient event = new EventNewEventClient();

	@Test(description = "Test to create new Ticket")
	public void createNewTicketTest() throws FileNotFoundException, IOException {
		event.createNewEventTest();
		response = createClientRequest()
				.path(readProperties("createTicket"))
				.request()
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.post(Entity.entity(DataPayload.getTicketData(newEventId), MediaType.valueOf("application/vnd.api+json")));
		String message = response.readEntity(String.class);
		System.out.println("Reponse Status : " + response.getStatus());
		System.out.println("Message details : " + message);
		System.out.println("Ticket Id : " + JsonPath.parse(message).read("data.id"));
		newTicketId = JsonPath.parse(message).read("data.id");
		Assert.assertEquals(response.getStatus(), 201);
		Reporter.log("New Ticket is created successfully");
	}

	@Test(enabled=false,description = "Test to update created Ticket")
	public void updateTicketTest() throws FileNotFoundException, IOException {
		response = createClientRequest()
				.path(readProperties("updateTicket")+newTicketId)
				.request()
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.put(Entity.entity(DataPayload.updateTicketData(newTicketId), MediaType.valueOf("application/vnd.api+json")));
		String message = response.readEntity(String.class);
		System.out.println("Reponse Status : " + response.getStatus());
		System.out.println("Message details : " + message);
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("Ticket updated successfully");
	}
	
	@Test(description = "Test to get ticket details")
	public void createdTicketDetailsTest() throws FileNotFoundException, IOException {
		response = createClientRequest()
				.path(readProperties("getSpecificTicket")+newTicketId)
				.request()
				.header("Authorization",jwtToken)
				.get(Response.class);
		String message = response.readEntity(String.class);
		System.out.println("Reponse Status : " + response.getStatus());
		System.out.println("Message details : " + message);
		Assert.assertEquals(response.getStatus(),200);
		Reporter.log("Ticket details displayed successfully");
	}
	
	@Test(description = "Test to delete Ticket")
	public void deleteTicketTest() throws FileNotFoundException, IOException {
		response = createClientRequest().path(readProperties("deleteTicket") + newTicketId).request()
				.header("Accept", "application/vnd.api+json").header("Authorization", jwtToken).delete();
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("Ticket deleted successfully");
	}

	@Test(description = "Test to delete created event details")
	public void deleteEventTest() throws FileNotFoundException, IOException {
		event.deleteEventTest();
	}
	
}
