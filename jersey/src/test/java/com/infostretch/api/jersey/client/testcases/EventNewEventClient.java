package com.infostretch.api.jersey.client.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.infostretch.api.jersey.payload.DataPayload;
import com.infostretch.api.jersey.testbase.TestBaseSpecification;
import com.jayway.jsonpath.JsonPath;

public class EventNewEventClient extends TestBaseSpecification{
	
	@Test(description = "Test to create new Event")
	public void createNewEventTest() throws FileNotFoundException, IOException {
		response=createClientRequest()
				.path(readProperties("createEvent"))
				.request() 
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.post(Entity.entity(DataPayload.getEventData(), MediaType.valueOf("application/vnd.api+json")));
				
				String message = response.readEntity(String.class);
				System.out.println("Reponse Status : " + response.getStatus());
				System.out.println("Message details : " + message);
				System.out.println("Event Id : " + JsonPath.parse(message).read("data.id"));
				newEventId=JsonPath.parse(message).read("data.id");
				
				Assert.assertEquals(response.getStatus(),201);
				Reporter.log("New Event is created successfully");
	}
	
	@Test(description = "Test to delete Event")
	public void deleteEventTest() throws FileNotFoundException, IOException {
		
		response=createClientRequest()
				.path(readProperties("deleteEvent") + newEventId)
				.request() 
				.header("Accept", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.delete();
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("Event deleted successfully"); 
	}
	

}
