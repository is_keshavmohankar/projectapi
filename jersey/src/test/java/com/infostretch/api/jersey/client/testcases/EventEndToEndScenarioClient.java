package com.infostretch.api.jersey.client.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.infostretch.api.jersey.payload.DataPayload;
import com.infostretch.api.jersey.testbase.TestBaseSpecification;
import com.jayway.jsonpath.JsonPath;

public class EventEndToEndScenarioClient extends TestBaseSpecification {
	
	EventAttendeesClient eventAttendees=new EventAttendeesClient();
	EventTicketClient eventTicket = new EventTicketClient();
	@Test(description = "Test to create new Order")
	public void createNewOrder() throws FileNotFoundException, IOException {
		eventAttendees.createNewAttendeesest();
		response=createClientRequest()
				.path(readProperties("getOrders"))
				.request() 
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.post(Entity.entity(DataPayload.getOrderJsonObject(newAttendeesId,newTicketId), MediaType.valueOf("application/vnd.api+json")));
				String message = response.readEntity(String.class);
				System.out.println("Reponse Status : " + response.getStatus());
				System.out.println("Message details : " + message);
				System.out.println("Order Id : " + JsonPath.parse(message).read("data.id"));
				newOrderId=JsonPath.parse(message).read("data.id");
				Assert.assertEquals(response.getStatus(),201);
				Reporter.log("Order created successfully");
	}
	
	@Test(description = "Test to delete specific order")
	public void deleteOrderTest() throws FileNotFoundException, IOException {
		response=createClientRequest()
				.path(readProperties("deleteOrders") + newOrderId)
				.request() 
				.header("Accept", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.delete();
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("Order deleted successfully"); 
	}
	
	@Test(description = "Test to delete Attendee")
	public void deleteAttendeesTest() throws FileNotFoundException, IOException {
		eventAttendees.deleteAttendeeTest();
	}
	
	@Test(description = "Test to delete ticketdetails")
	public void deleteTicketTest() {
		try {
			eventTicket.deleteTicketTest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test(description = "Test to delete event details")
	public void deleteEventTest() {
		try {
			eventTicket.deleteEventTest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
	
