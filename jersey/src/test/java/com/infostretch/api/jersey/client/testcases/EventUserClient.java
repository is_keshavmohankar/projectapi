package com.infostretch.api.jersey.client.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.infostretch.api.jersey.payload.DataPayload;
import com.infostretch.api.jersey.testbase.TestBaseSpecification;
import com.jayway.jsonpath.JsonPath;

public class EventUserClient extends TestBaseSpecification{
	
	@Test(description = "Test to get all user details")
	public void getUserTest() throws FileNotFoundException, IOException {
		response = createClientRequest()
				.path(readProperties("getUsers"))
				.request()
				.header("Authorization",jwtToken)
				.get(Response.class);
		String message = response.readEntity(String.class);
		System.out.println("Reponse Status : " + response.getStatus());
		System.out.println("Message details : " + message);
		Assert.assertEquals(response.getStatus(),200);
		Reporter.log("User detail displayed successfully");
	}
	
	@Test(description = "Test to create new user")
	public void createNewUserTest() throws FileNotFoundException, IOException {
		response=createClientRequest()
				.path(readProperties("createUser"))
				.request() 
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.post(Entity.entity(DataPayload.getUserJsonObject(), MediaType.valueOf("application/vnd.api+json")));
				
				String message = response.readEntity(String.class);
				System.out.println("Reponse Status : " + response.getStatus());
				System.out.println("Message details : " + message);
				System.out.println("User Id : " + JsonPath.parse(message).read("data.id"));
				newUserId=JsonPath.parse(message).read("data.id");
				
				Assert.assertEquals(response.getStatus(),201);
				Reporter.log("User is created successfully");
	}
			
	@Test(description = "Test to delete user details")
	public void deleteUserTest() throws FileNotFoundException, IOException {
		
		response=createClientRequest()
				.path(readProperties("getCreateduser") + newUserId)
				.request() 
				.header("Accept", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.delete();
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("User is deleted successfully"); 
	}
	
	
	
}
