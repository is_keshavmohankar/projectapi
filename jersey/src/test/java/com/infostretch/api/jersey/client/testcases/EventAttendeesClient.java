package com.infostretch.api.jersey.client.testcases;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.infostretch.api.jersey.payload.DataPayload;
import com.infostretch.api.jersey.testbase.TestBaseSpecification;
import com.jayway.jsonpath.JsonPath;

public class EventAttendeesClient extends TestBaseSpecification {
	
	EventTicketClient eventTicket = new EventTicketClient();
		
	@Test(description = "Test to create new Attendees")
	public void createNewAttendeesest() throws FileNotFoundException, IOException {
		eventTicket.createNewTicketTest();
		response=createClientRequest()
				.path(readProperties("createAttendees"))
				.request() 
				.header("Content-Type", "application/vnd.api+json")
				.header("Authorization", jwtToken)
				.post(Entity.entity(DataPayload.getAttendessData(newEventId,newTicketId), MediaType.valueOf("application/vnd.api+json")));
				String message = response.readEntity(String.class);
				System.out.println("Reponse Status : " + response.getStatus());
				System.out.println("Message details : " + message);
				System.out.println("Attendees Id : " + JsonPath.parse(message).read("data.id"));
				newAttendeesId=JsonPath.parse(message).read("data.id");
				Assert.assertEquals(response.getStatus(),201);
				Reporter.log("Attendee is created successfully");
	}
	
	@Test(enabled=true,description = "Test to get attendees details")
	public void createdAttendeesDetailsTest() throws FileNotFoundException, IOException {
		response = createClientRequest()
				.path(readProperties("getSpecificAttendees")+newAttendeesId)
				.request()
				.header("Authorization",jwtToken)
				.get(Response.class);
		String message = response.readEntity(String.class);
		System.out.println("Reponse Status : " + response.getStatus());
		System.out.println("Message details : " + message);
		Assert.assertEquals(response.getStatus(),200);
		Reporter.log("Attendees details displayed successfully");
	}
	@Test(description = "Test to delete Attendees details")
	public void deleteAttendeeTest() throws FileNotFoundException, IOException {
		
		response=createClientRequest()
				.path(readProperties("deleteAttendees") + newAttendeesId)
				.request() 
				.header("Accept", "application/vnd.api+json")
				.header("Authorization",jwtToken)
				.delete();
		Assert.assertEquals(response.getStatus(), 200);
		Reporter.log("Attendee is deleted successfully"); 
		
	}
	@Test(description = "Test to delete ticketdetails")
	public void deleteTicketTest() {
		try {
			eventTicket.deleteTicketTest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test(description = "Test to delete event details")
	public void deleteEventTest() {
		try {
			eventTicket.deleteEventTest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
